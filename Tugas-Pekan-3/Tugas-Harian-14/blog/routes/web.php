<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/master', function(){
    return view('adminlte.master');
});

route::get('/index', function(){
    return view('items.index');
});

route::get('/', function(){
    return view('adminlte.table');
});

route::get('/data-tables', function(){
    return view('adminlte.data-tables');
});