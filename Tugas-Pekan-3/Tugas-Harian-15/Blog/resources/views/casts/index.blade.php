@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <div class="card-header">
            <h3 class="card-title">Post Table</h3>
        </div>
        <div class="mt-2  ml-3">
            <a href="/casts/create"><input type="submit" class="btn btn-primary" value="Create New Post"></a>
        </div>
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <table class="table table-bordered">
            <thead>
                <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th style="width: 200px">Action</th>
                </tr>
            </thead>
            <tbody>
                <!-- @foreach($posts as $key => $hasil)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{ $hasil->title }} </td>
                    <td> {{ $hasil->body }} </td>
                    <td>Action</td>
                </tr>
                @endforeach -->

                @forelse($casts as $key => $cast)
                <tr>
                    <td> {{ $key +1 }} </td>
                    <td> {{ $cast->nama }} </td>
                    <td> {{ $cast->umur }} </td>
                    <td> {{ $cast->bio }} </td>
                    <td style="display:flex">
                        <a href="/casts/{{ $cast->id }}" class="btn btn-info btn-sm ml-1">show</a>
                        <a href="/casts/{{ $cast->id }}/edit" class="btn btn-info btn-sm ml-1">edit</a>
                        <form action="/casts/{{ $cast->id }}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm ml-1">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4" align="center">No Posts</td>
                </tr>
                @endforelse
            </tbody>
            </table>
        </div>
    </div>
@endsection