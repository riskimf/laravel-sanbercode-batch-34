@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
    <h2> {{ $post->title }} </h2>
    <p>{{ $post->body }}</p>
</div>
@endsection
