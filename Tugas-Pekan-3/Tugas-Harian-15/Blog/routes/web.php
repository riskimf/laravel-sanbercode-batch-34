<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/master', function(){
    return view('adminlte.master');
});

route::get('/index', function(){
    return view('items.index');
});

route::get('/', function(){
    return view('adminlte.table');
});

route::get('/data-tables', function(){
    return view('adminlte.data-tables');
});



route::get('/posts/create','PostController@create'); //membuat post post
route::post('/posts', 'PostController@store'); //menyimpan post
route::get('/posts','PostController@index'); //menampilkan semuapost
route::get('/posts/{id}','PostController@show'); //menampilkan post perbagian
route::get('/posts/{id}/edit','PostController@edit'); //mengedit post
route::put('/posts/{id}','PostController@update'); //mengupdate post
Route::delete('/posts/{id}', 'PostController@destroy'); //mendelete post

Route::get('/casts/create', 'CastController@create');
route::post('/casts', 'CastController@store');
route::get('/casts', 'CastController@index');
route::get('/casts/{id}', 'CastController@show');

