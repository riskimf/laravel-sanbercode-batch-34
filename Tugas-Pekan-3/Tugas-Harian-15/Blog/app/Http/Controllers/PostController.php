<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    // Menampilkan form
    public function create(){
        return view('posts.create');
    }

    // Memasukkan data Form ke Database
    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'title' => 'required|unique:posts',
            'body' => 'required',
        ]);

        $query = DB::table('posts') -> insert([
            "title" => $request['title'],
            "body" => $request['body']
        ]);

        // return redirect('/posts/create'); //redirect ke form pengisian
        // return redirect('/posts'); //redirect ke isi post
        return redirect('/posts')->with('success','Postingan Berhasil Disimpan!'); //redirect ke isi post dengan alert
    }

    // Memunculkan Data dari Database
    public function index(){
        $posts= DB::table('posts')->get(); //select * from post
        return view('posts.index',compact('posts'));
    }

    //Menampilkan Detail Post
    public function show($id){
        $post = DB::table('posts')->where('id', $id)->first();
        return view('posts.show',compact('post'));
    }

    //Menampilkan Form Edit 
    public function edit($id){
        $post = DB::table('posts')->where('id',$id)->first();
        return view('posts.edit',compact('post'));
    }

    //Menyimpan perubahan data (Update)
    // public function updates($id, Request $request){
        
    //     $request->validate([
    //         'title' => 'required|unique:posts',
    //         'body' => 'required',
    //     ]);

    //     $query = DB::table('posts')
    //                         ->where('id', $id)
    //                         ->update([
    //                             'title' => $request['title'],
    //                             'body' => $request['body']
    //                         ]);
    //     return redirect('/posts');
    // }

    public function update($id, Request $request){
        $request->validate([
            'title' => 'required|unique:posts',
            'body' => 'required'
        ]);

        $query = DB::table('posts')
                    ->where('id', $id)
                    ->update([
                        'title'=>$request['title'],
                        'body'=>$request['body']
                    ]);
        return redirect('/posts')->with('success','Berhasil Update Post');
    }

    //Menghapus data
    public function destroy($id){
        $query = DB::table('posts')->where('id',$id)->delete();
        return redirect('/posts')->with('success','Post Berhasil Dihapus');
    }
}
