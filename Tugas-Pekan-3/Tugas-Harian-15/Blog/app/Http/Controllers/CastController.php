<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CastController extends Controller
{
    public function create(){
        return view('/casts.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('pemeran') -> insert([
            "nama" => $request['nama'],
            "umur" => $request['umur'],
            "bio" => $request['bio']
        ]);
        return redirect('/casts')->with('success','Data Pemeran Berhasil Disimpan!');
    }

    public function index(){
        $casts = DB::table('pemeran')->get();
        return view('casts.index', compact('casts'));
    }
}
