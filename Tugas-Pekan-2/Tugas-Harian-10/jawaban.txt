Soal 1
>create database myshop

soal 2
>use database myshop;
>create table users(id int primary key auto_incerement, name varchar(255), email varchar(255), password varchar(255));

>create table categories(id int primary key auto_increment, nama varchar(255));

>create table items(id int primary key auto_increment, name varchar(255), description varchar(255), price int, stock int, category_id int, foreign key (category_id) references categories(id) on update cascade on delete cascade);

Soal 3
a. Users 
>insert into users(name, email, password) values
->('John Doe','john@doe.com','john123'),
->('Jane Doe','jane@doe.com','jenita123');

b. Category
> insert into categories(name) values
->('gadget'),('cloth'),('men'),('women'),('branded');

c. Items
> insert into items(name, description, price, stock, category_id) values
->('Sumsang b50','hape keren dari merk sumsang','4000000','100','1'),
->('Uniklooh','baju keren dari brand ternama','500000,'50','2'),
->('IMHO Watch','jam tangan anak yang jujur banget','10','1');

Soal 4
a. Mengambil semua data users
>select id, name, email from users;

b. Mengambil data items
• Query mengambil data dari tabel items dengan harga diatas 1000000
>select * from items where price > "1000000";
•Query mengambil data dari table items yang memiliki nama serupa
>select * from items where name like '%sang%';

c.Menampilkan data items dengan join dengan kategori
>select * from items inner join categories on items.category_id=categories.id;

5. Mengubah data dari Database
>update items set price='2500000' where name='Sumsang b50';

